/*
 * This file is generated by jOOQ.
*/
package hocdiban;


import hocdiban.tables.Lesson;
import hocdiban.tables.Question;
import hocdiban.tables.Result;
import hocdiban.tables.Test;
import hocdiban.tables.Typelesson;
import hocdiban.tables.User;

import javax.annotation.Generated;


/**
 * Convenience access to all tables in hocdiban_qldapm15
 */
@Generated(
    value = {
        "http://www.jooq.org",
        "jOOQ version:3.9.3"
    },
    comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class Tables {

    /**
     * The table <code>hocdiban_qldapm15.lesson</code>.
     */
    public static final Lesson LESSON = hocdiban.tables.Lesson.LESSON;

    /**
     * tập hợp các câu hỏi
     */
    public static final Question QUESTION = hocdiban.tables.Question.QUESTION;

    /**
     * kết quả các bài kiểm tra
     */
    public static final Result RESULT = hocdiban.tables.Result.RESULT;

    /**
     * The table <code>hocdiban_qldapm15.test</code>.
     */
    public static final Test TEST = hocdiban.tables.Test.TEST;

    /**
     * The table <code>hocdiban_qldapm15.typelesson</code>.
     */
    public static final Typelesson TYPELESSON = hocdiban.tables.Typelesson.TYPELESSON;

    /**
     * The table <code>hocdiban_qldapm15.user</code>.
     */
    public static final User USER = hocdiban.tables.User.USER;
}
