/*
 * This file is generated by jOOQ.
*/
package hocdiban.tables.records;


import hocdiban.tables.User;

import java.sql.Date;

import javax.annotation.Generated;

import org.jooq.Field;
import org.jooq.Record1;
import org.jooq.Record9;
import org.jooq.Row9;
import org.jooq.impl.UpdatableRecordImpl;


/**
 * This class is generated by jOOQ.
 */
@Generated(
    value = {
        "http://www.jooq.org",
        "jOOQ version:3.9.3"
    },
    comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class UserRecord extends UpdatableRecordImpl<UserRecord> implements Record9<String, String, String, String, String, Date, String, String, String> {

    private static final long serialVersionUID = 1668237238;

    /**
     * Setter for <code>hocdiban_qldapm15.user.UserName</code>.
     */
    public void setUsername(String value) {
        set(0, value);
    }

    /**
     * Getter for <code>hocdiban_qldapm15.user.UserName</code>.
     */
    public String getUsername() {
        return (String) get(0);
    }

    /**
     * Setter for <code>hocdiban_qldapm15.user.Password</code>.
     */
    public void setPassword(String value) {
        set(1, value);
    }

    /**
     * Getter for <code>hocdiban_qldapm15.user.Password</code>.
     */
    public String getPassword() {
        return (String) get(1);
    }

    /**
     * Setter for <code>hocdiban_qldapm15.user.Name</code>.
     */
    public void setName(String value) {
        set(2, value);
    }

    /**
     * Getter for <code>hocdiban_qldapm15.user.Name</code>.
     */
    public String getName() {
        return (String) get(2);
    }

    /**
     * Setter for <code>hocdiban_qldapm15.user.Role</code>.
     */
    public void setRole(String value) {
        set(3, value);
    }

    /**
     * Getter for <code>hocdiban_qldapm15.user.Role</code>.
     */
    public String getRole() {
        return (String) get(3);
    }

    /**
     * Setter for <code>hocdiban_qldapm15.user.Email</code>.
     */
    public void setEmail(String value) {
        set(4, value);
    }

    /**
     * Getter for <code>hocdiban_qldapm15.user.Email</code>.
     */
    public String getEmail() {
        return (String) get(4);
    }

    /**
     * Setter for <code>hocdiban_qldapm15.user.DayOfBirth</code>.
     */
    public void setDayofbirth(Date value) {
        set(5, value);
    }

    /**
     * Getter for <code>hocdiban_qldapm15.user.DayOfBirth</code>.
     */
    public Date getDayofbirth() {
        return (Date) get(5);
    }

    /**
     * Setter for <code>hocdiban_qldapm15.user.Address</code>.
     */
    public void setAddress(String value) {
        set(6, value);
    }

    /**
     * Getter for <code>hocdiban_qldapm15.user.Address</code>.
     */
    public String getAddress() {
        return (String) get(6);
    }

    /**
     * Setter for <code>hocdiban_qldapm15.user.Sexual</code>.
     */
    public void setSexual(String value) {
        set(7, value);
    }

    /**
     * Getter for <code>hocdiban_qldapm15.user.Sexual</code>.
     */
    public String getSexual() {
        return (String) get(7);
    }

    /**
     * Setter for <code>hocdiban_qldapm15.user.School</code>.
     */
    public void setSchool(String value) {
        set(8, value);
    }

    /**
     * Getter for <code>hocdiban_qldapm15.user.School</code>.
     */
    public String getSchool() {
        return (String) get(8);
    }

    // -------------------------------------------------------------------------
    // Primary key information
    // -------------------------------------------------------------------------

    /**
     * {@inheritDoc}
     */
    @Override
    public Record1<String> key() {
        return (Record1) super.key();
    }

    // -------------------------------------------------------------------------
    // Record9 type implementation
    // -------------------------------------------------------------------------

    /**
     * {@inheritDoc}
     */
    @Override
    public Row9<String, String, String, String, String, Date, String, String, String> fieldsRow() {
        return (Row9) super.fieldsRow();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Row9<String, String, String, String, String, Date, String, String, String> valuesRow() {
        return (Row9) super.valuesRow();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<String> field1() {
        return User.USER.USERNAME;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<String> field2() {
        return User.USER.PASSWORD;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<String> field3() {
        return User.USER.NAME;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<String> field4() {
        return User.USER.ROLE;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<String> field5() {
        return User.USER.EMAIL;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<Date> field6() {
        return User.USER.DAYOFBIRTH;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<String> field7() {
        return User.USER.ADDRESS;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<String> field8() {
        return User.USER.SEXUAL;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<String> field9() {
        return User.USER.SCHOOL;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String value1() {
        return getUsername();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String value2() {
        return getPassword();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String value3() {
        return getName();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String value4() {
        return getRole();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String value5() {
        return getEmail();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Date value6() {
        return getDayofbirth();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String value7() {
        return getAddress();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String value8() {
        return getSexual();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String value9() {
        return getSchool();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public UserRecord value1(String value) {
        setUsername(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public UserRecord value2(String value) {
        setPassword(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public UserRecord value3(String value) {
        setName(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public UserRecord value4(String value) {
        setRole(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public UserRecord value5(String value) {
        setEmail(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public UserRecord value6(Date value) {
        setDayofbirth(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public UserRecord value7(String value) {
        setAddress(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public UserRecord value8(String value) {
        setSexual(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public UserRecord value9(String value) {
        setSchool(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public UserRecord values(String value1, String value2, String value3, String value4, String value5, Date value6, String value7, String value8, String value9) {
        value1(value1);
        value2(value2);
        value3(value3);
        value4(value4);
        value5(value5);
        value6(value6);
        value7(value7);
        value8(value8);
        value9(value9);
        return this;
    }

    // -------------------------------------------------------------------------
    // Constructors
    // -------------------------------------------------------------------------

    /**
     * Create a detached UserRecord
     */
    public UserRecord() {
        super(User.USER);
    }

    /**
     * Create a detached, initialised UserRecord
     */
    public UserRecord(String username, String password, String name, String role, String email, Date dayofbirth, String address, String sexual, String school) {
        super(User.USER);

        set(0, username);
        set(1, password);
        set(2, name);
        set(3, role);
        set(4, email);
        set(5, dayofbirth);
        set(6, address);
        set(7, sexual);
        set(8, school);
    }
}
