/*
 * This file is generated by jOOQ.
*/
package hocdiban.tables;


import hocdiban.HocdibanQldapm15;
import hocdiban.Keys;
import hocdiban.tables.records.ResultRecord;

import java.util.Arrays;
import java.util.List;

import javax.annotation.Generated;

import org.jooq.Field;
import org.jooq.ForeignKey;
import org.jooq.Identity;
import org.jooq.Schema;
import org.jooq.Table;
import org.jooq.TableField;
import org.jooq.UniqueKey;
import org.jooq.impl.TableImpl;


/**
 * kết quả các bài kiểm tra
 */
@Generated(
    value = {
        "http://www.jooq.org",
        "jOOQ version:3.9.3"
    },
    comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class Result extends TableImpl<ResultRecord> {

    private static final long serialVersionUID = -1096521455;

    /**
     * The reference instance of <code>hocdiban_qldapm15.result</code>
     */
    public static final Result RESULT = new Result();

    /**
     * The class holding records for this type
     */
    @Override
    public Class<ResultRecord> getRecordType() {
        return ResultRecord.class;
    }

    /**
     * The column <code>hocdiban_qldapm15.result.ID</code>.
     */
    public final TableField<ResultRecord, Integer> ID = createField("ID", org.jooq.impl.SQLDataType.INTEGER.nullable(false), this, "");

    /**
     * The column <code>hocdiban_qldapm15.result.UserName</code>.
     */
    public final TableField<ResultRecord, String> USERNAME = createField("UserName", org.jooq.impl.SQLDataType.VARCHAR.length(20).nullable(false), this, "");

    /**
     * The column <code>hocdiban_qldapm15.result.TestID</code>.
     */
    public final TableField<ResultRecord, Integer> TESTID = createField("TestID", org.jooq.impl.SQLDataType.INTEGER.nullable(false), this, "");

    /**
     * The column <code>hocdiban_qldapm15.result.Point</code>.
     */
    public final TableField<ResultRecord, Double> POINT = createField("Point", org.jooq.impl.SQLDataType.FLOAT.nullable(false), this, "");

    /**
     * The column <code>hocdiban_qldapm15.result.Answer</code>.
     */
    public final TableField<ResultRecord, String> ANSWER = createField("Answer", org.jooq.impl.SQLDataType.VARCHAR.length(500).nullable(false), this, "");

    /**
     * Create a <code>hocdiban_qldapm15.result</code> table reference
     */
    public Result() {
        this("result", null);
    }

    /**
     * Create an aliased <code>hocdiban_qldapm15.result</code> table reference
     */
    public Result(String alias) {
        this(alias, RESULT);
    }

    private Result(String alias, Table<ResultRecord> aliased) {
        this(alias, aliased, null);
    }

    private Result(String alias, Table<ResultRecord> aliased, Field<?>[] parameters) {
        super(alias, null, aliased, parameters, "kết quả các bài kiểm tra");
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Schema getSchema() {
        return HocdibanQldapm15.HOCDIBAN_QLDAPM15;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Identity<ResultRecord, Integer> getIdentity() {
        return Keys.IDENTITY_RESULT;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public UniqueKey<ResultRecord> getPrimaryKey() {
        return Keys.KEY_RESULT_PRIMARY;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<UniqueKey<ResultRecord>> getKeys() {
        return Arrays.<UniqueKey<ResultRecord>>asList(Keys.KEY_RESULT_PRIMARY);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<ForeignKey<ResultRecord, ?>> getReferences() {
        return Arrays.<ForeignKey<ResultRecord, ?>>asList(Keys.USER_NAME, Keys.TEST_ID);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Result as(String alias) {
        return new Result(alias, this);
    }

    /**
     * Rename this table
     */
    @Override
    public Result rename(String name) {
        return new Result(name, null);
    }
}
