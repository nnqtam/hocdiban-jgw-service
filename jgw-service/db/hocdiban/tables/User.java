/*
 * This file is generated by jOOQ.
*/
package hocdiban.tables;


import hocdiban.HocdibanQldapm15;
import hocdiban.Keys;
import hocdiban.tables.records.UserRecord;

import java.sql.Date;
import java.util.Arrays;
import java.util.List;

import javax.annotation.Generated;

import org.jooq.Field;
import org.jooq.Schema;
import org.jooq.Table;
import org.jooq.TableField;
import org.jooq.UniqueKey;
import org.jooq.impl.TableImpl;


/**
 * This class is generated by jOOQ.
 */
@Generated(
    value = {
        "http://www.jooq.org",
        "jOOQ version:3.9.3"
    },
    comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class User extends TableImpl<UserRecord> {

    private static final long serialVersionUID = 1573402844;

    /**
     * The reference instance of <code>hocdiban_qldapm15.user</code>
     */
    public static final User USER = new User();

    /**
     * The class holding records for this type
     */
    @Override
    public Class<UserRecord> getRecordType() {
        return UserRecord.class;
    }

    /**
     * The column <code>hocdiban_qldapm15.user.UserName</code>.
     */
    public final TableField<UserRecord, String> USERNAME = createField("UserName", org.jooq.impl.SQLDataType.VARCHAR.length(20).nullable(false), this, "");

    /**
     * The column <code>hocdiban_qldapm15.user.Password</code>.
     */
    public final TableField<UserRecord, String> PASSWORD = createField("Password", org.jooq.impl.SQLDataType.VARCHAR.length(100).nullable(false), this, "");

    /**
     * The column <code>hocdiban_qldapm15.user.Name</code>.
     */
    public final TableField<UserRecord, String> NAME = createField("Name", org.jooq.impl.SQLDataType.VARCHAR.length(50).nullable(false), this, "");

    /**
     * The column <code>hocdiban_qldapm15.user.Role</code>.
     */
    public final TableField<UserRecord, String> ROLE = createField("Role", org.jooq.impl.SQLDataType.VARCHAR.length(10).nullable(false), this, "");

    /**
     * The column <code>hocdiban_qldapm15.user.Email</code>.
     */
    public final TableField<UserRecord, String> EMAIL = createField("Email", org.jooq.impl.SQLDataType.VARCHAR.length(50), this, "");

    /**
     * The column <code>hocdiban_qldapm15.user.DayOfBirth</code>.
     */
    public final TableField<UserRecord, Date> DAYOFBIRTH = createField("DayOfBirth", org.jooq.impl.SQLDataType.DATE, this, "");

    /**
     * The column <code>hocdiban_qldapm15.user.Address</code>.
     */
    public final TableField<UserRecord, String> ADDRESS = createField("Address", org.jooq.impl.SQLDataType.VARCHAR.length(150), this, "");

    /**
     * The column <code>hocdiban_qldapm15.user.Sexual</code>.
     */
    public final TableField<UserRecord, String> SEXUAL = createField("Sexual", org.jooq.impl.SQLDataType.VARCHAR.length(10), this, "");

    /**
     * The column <code>hocdiban_qldapm15.user.School</code>.
     */
    public final TableField<UserRecord, String> SCHOOL = createField("School", org.jooq.impl.SQLDataType.VARCHAR.length(100), this, "");

    /**
     * Create a <code>hocdiban_qldapm15.user</code> table reference
     */
    public User() {
        this("user", null);
    }

    /**
     * Create an aliased <code>hocdiban_qldapm15.user</code> table reference
     */
    public User(String alias) {
        this(alias, USER);
    }

    private User(String alias, Table<UserRecord> aliased) {
        this(alias, aliased, null);
    }

    private User(String alias, Table<UserRecord> aliased, Field<?>[] parameters) {
        super(alias, null, aliased, parameters, "");
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Schema getSchema() {
        return HocdibanQldapm15.HOCDIBAN_QLDAPM15;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public UniqueKey<UserRecord> getPrimaryKey() {
        return Keys.KEY_USER_PRIMARY;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<UniqueKey<UserRecord>> getKeys() {
        return Arrays.<UniqueKey<UserRecord>>asList(Keys.KEY_USER_PRIMARY);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public User as(String alias) {
        return new User(alias, this);
    }

    /**
     * Rename this table
     */
    @Override
    public User rename(String name) {
        return new User(name, null);
    }
}
