/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.admin.service;

import app.config.ConfigApp;
import core.utilities.DBConnector;
import static hocdiban.tables.Typelesson.TYPELESSON;
import java.sql.Connection;
import java.util.List;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import org.apache.log4j.Logger;
import org.jooq.DSLContext;
import org.jooq.Record;
import org.jooq.SQLDialect;
import org.jooq.impl.DSL;

/**
 *
 * @author tam
 */
public class TypeLessonService {

    private static final Logger logger = Logger.getLogger(TypeLessonService.class.getName());
    private static final Lock LOCK = new ReentrantLock();
    private static TypeLessonService instance = null;

    private final DBConnector dbConnector;

    private TypeLessonService() {
        this.dbConnector = DBConnector.getInstance(ConfigApp.MYSQL_HOST,
                ConfigApp.MYSQL_PORT,
                ConfigApp.MYSQL_DBNAME,
                ConfigApp.MYSQL_USER,
                ConfigApp.MYSQL_PASSWORD);
    }

    public static TypeLessonService getInstance() {
        if (instance == null) {
            LOCK.lock();
            try {
                if (instance == null) {
                    instance = new TypeLessonService();
                }
            } finally {
                LOCK.unlock();
            }
        }
        return instance;
    }

    public Record insertTypeLesson(String name) {
        Connection conn = null;
        try {
            conn = dbConnector.getMySqlConnection();
            DSLContext create = DSL.using(conn, SQLDialect.MYSQL);

            Record record = create.insertInto(TYPELESSON, TYPELESSON.NAME)
                    .values(name)
                    .returning(TYPELESSON.ID)
                    .fetchOne();

            if (record != null) {
                logger.info("Insert type lesson success");
                return record;
            } else {
                logger.error("Insert type lesson fail");
                return null;
            }
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
        } finally {
            if (conn != null) {
                dbConnector.close(conn);
            }
        }
        logger.error("Database Error");
        return null;
    }

    public List<String> getTypeLessonList() {
        Connection conn = null;
        try {
            conn = dbConnector.getMySqlConnection();
            DSLContext create = DSL.using(conn, SQLDialect.MYSQL);

            List<String> listName = create.select().from(TYPELESSON).fetch(TYPELESSON.NAME);

            if (listName != null) {
                logger.info("Get type lesson list success");
                return listName;
            } else {
                logger.error("Get type lesson list fail");
                return null;
            }
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
        } finally {
            if (conn != null) {
                dbConnector.close(conn);
            }
        }
        logger.error("Database Error");
        return null;
    }
}
