/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.admin.service;

import app.config.ConfigApp;
import core.utilities.DBConnector;
import static hocdiban.tables.Question.QUESTION;
import java.sql.Connection;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import org.apache.log4j.Logger;
import org.jooq.DSLContext;
import org.jooq.Record;
import org.jooq.SQLDialect;
import org.jooq.impl.DSL;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author tam
 */
public class QuestionService {

    private static final Logger logger = Logger.getLogger(QuestionService.class.getName());
    private static final Lock LOCK = new ReentrantLock();
    private static QuestionService instance = null;

    private final DBConnector dbConnector;

    private QuestionService() {
        this.dbConnector = DBConnector.getInstance(ConfigApp.MYSQL_HOST,
                ConfigApp.MYSQL_PORT,
                ConfigApp.MYSQL_DBNAME,
                ConfigApp.MYSQL_USER,
                ConfigApp.MYSQL_PASSWORD);
    }

    public static QuestionService getInstance() {
        if (instance == null) {
            LOCK.lock();
            try {
                if (instance == null) {
                    instance = new QuestionService();
                }
            } finally {
                LOCK.unlock();
            }
        }
        return instance;
    }

    public Record insertQuestion(JSONObject question) {
        Connection conn = null;
        try {
            conn = dbConnector.getMySqlConnection();
            DSLContext create = DSL.using(conn, SQLDialect.MYSQL);
            String type = question.getString("type");
            String correctAnswer = question.getString("correctAnswer");
            question.remove("type");
            question.remove("correctAnswer");
            String content = question.toString();

            Record record = create.insertInto(QUESTION, QUESTION.TYPE, QUESTION.CORRECTANSWER, QUESTION.CONTENT)
                    .values(type, correctAnswer, content)
                    .returning(QUESTION.ID)
                    .fetchOne();
            if (record != null) {
                logger.info("insert question success");
                return record;
            } else {
                logger.error("Insert question fail");
                return null;
            }

        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
        } finally {
            if (conn != null) {
                dbConnector.close(conn);
            }
        }
        logger.error("Database Error");
        return null;
    }

    public boolean validateQuestion(JSONObject question) {
        try {
            switch (question.getString("type")) {
                case "CHOOSE_WORD":
                    return validateChooseWord(question);
                case "PRONUNCIATION":
                    return validatePronunciation(question);
                case "CHOOSE_PASSAGE":
                    return validateChoosePassage(question);
                case "FIND_MISTAKE":
                    return validateFindMistake(question);
                case "REWRITE_1":
                    return validateRewrite1(question);
                case "REWRITE_2":
                    return validateRewrite2(question);
                default:
                    logger.error("Question type isn't exist - " + question.toString());
                    return false;
            }

        } catch (Exception ex) {
            logger.error("Question type can't get - " + question.toString(), ex);
        }
        return false;
    }

    private boolean validateChooseWord(JSONObject question) {
        if (!question.has("content")) {
            logger.error("Question content isn't exist - " + question.toString());
            return false;
        }
        if (!question.has("correctAnswer")) {
            logger.error("Question correctAnswer isn't exist - " + question.toString());
            return false;
        }
        if (question.has("answer")) {
            try {
                JSONObject answer = question.getJSONObject("answer");
                if (!answer.has("A") || !answer.has("B") || !answer.has("C") || !answer.has("D")) {
                    return false;
                }
            } catch (Exception ex) {
                logger.error("Question answer can't parse" + question.toString(), ex);
                return false;
            }
        } else {
            return false;
        }
        return true;
    }

    private boolean validatePronunciation(JSONObject question) {
        if (!question.has("correctAnswer")) {
            logger.error("Question correctAnswer isn't exist - " + question.toString());
            return false;
        }
        if (question.has("answer")) {
            try {
                JSONObject answer = question.getJSONObject("answer");
                if (!answer.has("A") || !answer.has("B") || !answer.has("C") || !answer.has("D")) {
                    return false;
                }
            } catch (Exception ex) {
                logger.error("Question answer can't parse - " + question.toString(), ex);
                return false;
            }
        } else {
            return false;
        }
        return true;
    }

    private boolean validateChoosePassage(JSONObject question) {
        if (!question.has("passage")) {
            logger.error("Question passage isn't exist - " + question.toString());
            return false;
        }
        if (question.has("questions")) {
            try {
                JSONArray questions = question.getJSONArray("questions");
                for (int i = 0; i < questions.length(); i++) {
                    JSONObject obj = questions.getJSONObject(i);
                    if (!obj.has("content")) {
                        return false;
                    }
                    if (obj.has("answer")) {
                        try {
                            JSONObject answer = obj.getJSONObject("answer");
                            if (!answer.has("A") || !answer.has("B") || !answer.has("C") || !answer.has("D")) {
                                return false;
                            }
                        } catch (Exception ex) {
                            logger.error("Question answer can't parse - " + question.toString(), ex);
                            return false;
                        }
                    } else {
                        return false;
                    }
                    if (!obj.has("correctAnswer")) {
                        logger.error("Question in passage isn't have correctAnswer - " + question.toString());
                        return false;
                    }
                }
            } catch (Exception ex) {
                logger.error("Question questions can't parse" + question.toString(), ex);
                return false;
            }
        } else {
            logger.error("Question correctAnswer isn't exist - " + question.toString());
            return false;
        }
        return true;
    }

    private boolean validateFindMistake(JSONObject question) {
        if (!question.has("content")) {
            logger.error("Question content isn't exist - " + question.toString());
            return false;
        }
        if (!question.has("correctAnswer")) {
            logger.error("Question correctAnswer isn't exist - " + question.toString());
            return false;
        }
        return true;
    }

    private boolean validateRewrite1(JSONObject question) {
        if (!question.has("content")) {
            logger.error("Question content isn't exist - " + question.toString());
            return false;
        }
        if (!question.has("correctAnswer")) {
            logger.error("Question correctAnswer isn't exist - " + question.toString());
            return false;
        }
        return true;
    }

    private boolean validateRewrite2(JSONObject question) {
        if (!question.has("content")) {
            logger.error("Question content isn't exist - " + question.toString());
            return false;
        }
        if (!question.has("correctAnswer")) {
            logger.error("Question correctAnswer isn't exist - " + question.toString());
            return false;
        }
        if (!question.has("startWith")) {
            logger.error("Question startWith isn't exist - " + question.toString());
            return false;
        }
        return true;
    }
}
