/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.admin.service;

import app.config.ConfigApp;
import core.utilities.DBConnector;
import static hocdiban.tables.Lesson.LESSON;
import java.sql.Connection;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import org.apache.log4j.Logger;
import org.jooq.DSLContext;
import org.jooq.Record;
import org.jooq.SQLDialect;
import org.jooq.impl.DSL;

/**
 *
 * @author tam
 */
public class LessonService {

    private static final Logger logger = Logger.getLogger(LessonService.class.getName());
    private static final Lock LOCK = new ReentrantLock();
    private static LessonService instance = null;

    private final DBConnector dbConnector;

    private LessonService() {
        this.dbConnector = DBConnector.getInstance(ConfigApp.MYSQL_HOST,
                ConfigApp.MYSQL_PORT,
                ConfigApp.MYSQL_DBNAME,
                ConfigApp.MYSQL_USER,
                ConfigApp.MYSQL_PASSWORD);
    }

    public static LessonService getInstance() {
        if (instance == null) {
            LOCK.lock();
            try {
                if (instance == null) {
                    instance = new LessonService();
                }
            } finally {
                LOCK.unlock();
            }
        }
        return instance;
    }

    public Record insertLesson(String type, String name, String content, int importance) {
        Connection conn = null;
        try {
            conn = dbConnector.getMySqlConnection();
            DSLContext create = DSL.using(conn, SQLDialect.MYSQL);

            Record record = create.insertInto(LESSON, LESSON.TYPE, LESSON.NAME, LESSON.CONTENT, LESSON.IMPORTANCE)
                    .values(type, name, content, importance)
                    .returning(LESSON.ID)
                    .fetchOne();

            if (record != null) {
                logger.info("insert lesson success");
                return record;
            } else {
                logger.error("insert lesson fail");
                return null;
            }
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
        } finally {
            if (conn != null) {
                dbConnector.close(conn);
            }
        }
        logger.error("Database Error");
        return null;
    }
}
