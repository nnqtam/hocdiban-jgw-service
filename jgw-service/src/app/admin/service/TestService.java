/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.admin.service;

import app.config.ConfigApp;
import core.utilities.DBConnector;
import static hocdiban.Tables.TEST;
import static hocdiban.tables.Question.QUESTION;
import java.sql.Connection;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import org.apache.log4j.Logger;
import org.jooq.DSLContext;
import org.jooq.Record;
import org.jooq.SQLDialect;
import org.jooq.impl.DSL;
import org.json.JSONArray;

/**
 *
 * @author tam
 */
public class TestService {

    private static final Logger logger = Logger.getLogger(TestService.class.getName());
    private static final Lock LOCK = new ReentrantLock();
    private static TestService instance = null;

    private final DBConnector dbConnector;

    private TestService() {
        this.dbConnector = DBConnector.getInstance(ConfigApp.MYSQL_HOST,
                ConfigApp.MYSQL_PORT,
                ConfigApp.MYSQL_DBNAME,
                ConfigApp.MYSQL_USER,
                ConfigApp.MYSQL_PASSWORD);
    }

    public static TestService getInstance() {
        if (instance == null) {
            LOCK.lock();
            try {
                if (instance == null) {
                    instance = new TestService();
                }
            } finally {
                LOCK.unlock();
            }
        }
        return instance;
    }

    public int insertTest(JSONArray questionList, String name) {
        Connection conn = null;
        try {
            conn = dbConnector.getMySqlConnection();
            DSLContext create = DSL.using(conn, SQLDialect.MYSQL);
            JSONArray content = new JSONArray();

            for (int i = 0; i < questionList.length(); i++) {
                if (!QuestionService.getInstance().validateQuestion(questionList.getJSONObject(i))) {
                    return -1;
                }
            }
            for (int i = 0; i < questionList.length(); i++) {
                Record result = QuestionService.getInstance().insertQuestion(questionList.getJSONObject(i));
                if (result != null) {
                    content.put(result.getValue(QUESTION.ID));
                } else {
                    return -2;
                }
            }

            Record record = create.insertInto(TEST, TEST.NAME, TEST.CONTENT)
                    .values(name, content.toString())
                    .returning(TEST.ID)
                    .fetchOne();
            if (record != null) {
                return 1;
            } else {
                return -3;
            }
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
        } finally {
            if (conn != null) {
                dbConnector.close(conn);
            }
        }
        logger.error("Database Error");
        return -4;
    }
}
