/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.admin.controller;

import app.admin.service.TestService;
import app.entity.EnApiOutput;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.json.JSONArray;

/**
 *
 * @author tam
 */
public class TestController extends AbstractAdminController {

    private final Logger logger = Logger.getLogger(TestController.class);

    @Override
    protected EnApiOutput doProcess(String action, HttpServletRequest req, HttpServletResponse resp) {
        try {
            switch (action) {
                case "insertTest":
                    return insertTest(req, resp);
                default:
                    return new EnApiOutput(EnApiOutput.ERROR_CODE_API.UNSUPPORTED_ERROR);
            }
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return new EnApiOutput(EnApiOutput.ERROR_CODE_API.SERVER_ERROR);
        }
    }

    private EnApiOutput insertTest(HttpServletRequest req, HttpServletResponse resp) {
        try {
            if (!checkValidParam(req, new String[]{"content", "name"})) {
                logger.info("insertTest fail: " + req);
                return new EnApiOutput(EnApiOutput.ERROR_CODE_API.INVALID_DATA_INPUT);
            }
            JSONArray questionList = new JSONArray(req.getParameter("content"));
            String name = req.getParameter("name");
            int result = TestService.getInstance().insertTest(questionList, name);
            switch (result) {

                case 1:
                    return new EnApiOutput(EnApiOutput.ERROR_CODE_API.SUCCESS, name);
                case -1:
                    return new EnApiOutput(EnApiOutput.ERROR_CODE_API.QUESTION_DATA_INVALID);
                case -2:
                    return new EnApiOutput(EnApiOutput.ERROR_CODE_API.INSERT_QUESTION_FAIL);
                case -3:
                    return new EnApiOutput(EnApiOutput.ERROR_CODE_API.INSERT_TEST_FAIL);
                case -4:
                    return new EnApiOutput(EnApiOutput.ERROR_CODE_API.DATABASE_ERROR);
                default:
                    return new EnApiOutput(EnApiOutput.ERROR_CODE_API.SERVER_ERROR);
            }

        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
            return new EnApiOutput(EnApiOutput.ERROR_CODE_API.TEST_CONTENT_INVALID);
        }
    }
}
