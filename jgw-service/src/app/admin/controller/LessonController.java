/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.admin.controller;

import app.admin.service.LessonService;
import app.entity.EnApiOutput;
import core.utilities.CommonUtil;
import static hocdiban.tables.Lesson.LESSON;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.jooq.Record;

/**
 *
 * @author tam
 */
public class LessonController extends AbstractAdminController {

    private final Logger logger = Logger.getLogger(LessonController.class);

    @Override
    protected EnApiOutput doProcess(String action, HttpServletRequest req, HttpServletResponse resp) {
        try {
            switch (action) {
                case "insertLesson":
                    return insertLesson(req, resp);
                default:
                    return new EnApiOutput(EnApiOutput.ERROR_CODE_API.UNSUPPORTED_ERROR);
            }
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return new EnApiOutput(EnApiOutput.ERROR_CODE_API.SERVER_ERROR);
        }
    }

    private EnApiOutput insertLesson(HttpServletRequest req, HttpServletResponse resp) {
        try {
            if (!checkValidParam(req, new String[]{"type", "name", "content", "importance"})
                    || !CommonUtil.isInteger(req.getParameter("importance"))) {
                logger.info("insertLesson fail: " + req.toString());
                return new EnApiOutput(EnApiOutput.ERROR_CODE_API.INVALID_DATA_INPUT);
            }
            String type = req.getParameter("type");
            String name = req.getParameter("name");
            String content = req.getParameter("content");
            int importance = Integer.parseInt(req.getParameter("importance"));
            Record result = LessonService.getInstance().insertLesson(type, name, content, importance);
            if (result != null) {
                return new EnApiOutput(EnApiOutput.ERROR_CODE_API.SUCCESS, result.getValue(LESSON.ID));
            } else {
                return new EnApiOutput(EnApiOutput.ERROR_CODE_API.INSERT_LESSON_FAIL);
            }

        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
            return new EnApiOutput(EnApiOutput.ERROR_CODE_API.SERVER_ERROR);
        }
    }
}
