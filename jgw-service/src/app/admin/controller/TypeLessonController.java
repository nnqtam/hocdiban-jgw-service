/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.admin.controller;

import app.admin.service.TypeLessonService;
import app.entity.EnApiOutput;
import static hocdiban.tables.Lesson.LESSON;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.jooq.Record;

/**
 *
 * @author tam
 */
public class TypeLessonController extends AbstractAdminController {

    private final Logger logger = Logger.getLogger(TypeLessonController.class);

    @Override
    protected EnApiOutput doProcess(String action, HttpServletRequest req, HttpServletResponse resp) {
        try {
            switch (action) {
                case "insertTypeLesson":
                    return insertTypeLesson(req, resp);
                case "getTypeLessonList":
                    return getTypeLessonList(req, resp);
                default:
                    return new EnApiOutput(EnApiOutput.ERROR_CODE_API.UNSUPPORTED_ERROR);
            }
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return new EnApiOutput(EnApiOutput.ERROR_CODE_API.SERVER_ERROR);
        }
    }

    private EnApiOutput insertTypeLesson(HttpServletRequest req, HttpServletResponse resp) {
        try {
            if (!checkValidParam(req, new String[]{"name"})) {
                logger.info("insertTypeLesson fail: " + req.toString());
                return new EnApiOutput(EnApiOutput.ERROR_CODE_API.INVALID_DATA_INPUT);
            }
            String name = req.getParameter("name");
            Record result = TypeLessonService.getInstance().insertTypeLesson(name);
            if (result != null) {
                return new EnApiOutput(EnApiOutput.ERROR_CODE_API.SUCCESS, result.getValue(LESSON.ID));
            } else {
                return new EnApiOutput(EnApiOutput.ERROR_CODE_API.INSERT_TYPE_LESSON_FAIL);
            }

        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
            return new EnApiOutput(EnApiOutput.ERROR_CODE_API.SERVER_ERROR);
        }
    }

    private EnApiOutput getTypeLessonList(HttpServletRequest req, HttpServletResponse resp) {
        try {
            List<String> result = TypeLessonService.getInstance().getTypeLessonList();
            if (result != null) {
                return new EnApiOutput(EnApiOutput.ERROR_CODE_API.SUCCESS, result);
            } else {
                return new EnApiOutput(EnApiOutput.ERROR_CODE_API.GET_LIST_TYPE_LESSON_FAIL);
            }
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
            return new EnApiOutput(EnApiOutput.ERROR_CODE_API.SERVER_ERROR);
        }
    }
}
