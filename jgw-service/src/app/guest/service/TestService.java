/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.guest.service;

import app.config.ConfigApp;
import core.utilities.DBConnector;
import static hocdiban.Tables.TEST;
import java.sql.Connection;
import java.util.Map;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import org.apache.log4j.Logger;
import org.jooq.DSLContext;
import org.jooq.SQLDialect;
import org.jooq.impl.DSL;

/**
 *
 * @author tam
 */
public class TestService {

    private static final Logger logger = Logger.getLogger(TestService.class.getName());
    private static final Lock LOCK = new ReentrantLock();
    private static TestService instance = null;

    private final DBConnector dbConnector;

    private TestService() {
        this.dbConnector = DBConnector.getInstance(ConfigApp.MYSQL_HOST,
                ConfigApp.MYSQL_PORT,
                ConfigApp.MYSQL_DBNAME,
                ConfigApp.MYSQL_USER,
                ConfigApp.MYSQL_PASSWORD);
    }

    public static TestService getInstance() {
        if (instance == null) {
            LOCK.lock();
            try {
                if (instance == null) {
                    instance = new TestService();
                }
            } finally {
                LOCK.unlock();
            }
        }
        return instance;
    }

    public Map<Integer, String> getTestList(int offset, int limit) {
        Connection conn = null;
        try {
            conn = dbConnector.getMySqlConnection();
            DSLContext create = DSL.using(conn, SQLDialect.MYSQL);
            Map<Integer, String> result = create.selectFrom(TEST)
                    .offset(offset)
                    .limit(limit)
                    .fetchMap(TEST.ID, TEST.NAME);
            if (result != null) {
                return result;
            } else {
                logger.info("can not get ");
                return null;
            }
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
        } finally {
            if (conn != null) {
                dbConnector.close(conn);
            }
        }
        logger.error("Database Error");
        return null;
    }

}
