/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.guest.service;

import app.config.ConfigApp;
import app.entity.EnApp.LessonInfo;
import core.utilities.DBConnector;
import static hocdiban.tables.Lesson.LESSON;
import hocdiban.tables.records.LessonRecord;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import org.apache.log4j.Logger;
import org.jooq.DSLContext;
import org.jooq.Result;
import org.jooq.SQLDialect;
import org.jooq.impl.DSL;

/**
 *
 * @author tam
 */
public class LessonService {

    private static final Logger logger = Logger.getLogger(LessonService.class.getName());
    private static final Lock LOCK = new ReentrantLock();
    private static LessonService instance = null;

    private final DBConnector dbConnector;

    private LessonService() {
        this.dbConnector = DBConnector.getInstance(ConfigApp.MYSQL_HOST,
                ConfigApp.MYSQL_PORT,
                ConfigApp.MYSQL_DBNAME,
                ConfigApp.MYSQL_USER,
                ConfigApp.MYSQL_PASSWORD);
    }

    public static LessonService getInstance() {
        if (instance == null) {
            LOCK.lock();
            try {
                if (instance == null) {
                    instance = new LessonService();
                }
            } finally {
                LOCK.unlock();
            }
        }
        return instance;
    }

    public ArrayList<LessonInfo> getLessonList() {
        Connection conn = null;
        try {
            conn = dbConnector.getMySqlConnection();
            DSLContext create = DSL.using(conn, SQLDialect.MYSQL);
            ArrayList<LessonInfo> result = new ArrayList<LessonInfo>();
            Result<LessonRecord> record = create.selectFrom(LESSON).fetch();
            if (record != null) {
                for (LessonRecord lesson : record) {
                    result.add(new LessonInfo(lesson.getId(), lesson.getType(), lesson.getImportance(), lesson.getName()));
                }
                logger.info("get list lesson success");
                return result;
            } else {
                logger.error("get list lesson fail, data is empty");
                return null;
            }

        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
        } finally {
            if (conn != null) {
                dbConnector.close(conn);
            }
        }
        logger.error("Database Error");
        return null;
    }
}
