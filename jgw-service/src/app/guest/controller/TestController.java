/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.guest.controller;

import app.guest.service.TestService;
import app.entity.EnApiOutput;
import app.guest.service.VerifyUserNameService;
import app.config.ConfigApp;
import core.controller.ApiServlet;
import core.utilities.CommonUtil;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;

/**
 *
 * @author tam
 */
public class TestController extends ApiServlet {

    private final Logger logger = Logger.getLogger(TestController.class);
    VerifyUserNameService verifyInstance = VerifyUserNameService.getInstance(ConfigApp.LOGIN_SECRET_KEY);

    @Override
    protected EnApiOutput execute(HttpServletRequest req, HttpServletResponse resp) {
        try {
            if (!checkValidParam(req, new String[]{"action"})
                    || !CommonUtil.isValidString(req.getParameter("action"))) {
                logger.info("INVALID_DATA_INPUT ( Parameter : action): " + resp);
                return new EnApiOutput(EnApiOutput.ERROR_CODE_API.INVALID_DATA_INPUT);
            }
            String action = req.getParameter("action");
            switch (action) {
                case "getTestList":
                    return getTestList(req, resp);

                default:
                    return new EnApiOutput(EnApiOutput.ERROR_CODE_API.UNSUPPORTED_ERROR);
            }
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
        }
        return new EnApiOutput(EnApiOutput.ERROR_CODE_API.SERVER_ERROR);
    }

    private EnApiOutput getTestList(HttpServletRequest req, HttpServletResponse resp) {
        try {
            if (!checkValidParam(req, new String[]{"offset", "limit"})
                    || !CommonUtil.isInteger(req.getParameter("offset"))
                    || !CommonUtil.isInteger(req.getParameter("limit"))) {
                logger.info("insertTest fail: " + req);
                return new EnApiOutput(EnApiOutput.ERROR_CODE_API.INVALID_DATA_INPUT);
            }
            int offset = Integer.parseInt(req.getParameter("offset"));
            int limit = Integer.parseInt(req.getParameter("limit"));
            if (offset < 0 || limit < 1) {
                return new EnApiOutput(EnApiOutput.ERROR_CODE_API.OFFSET_OR_LIMIT_INVALID);
            }
            Map<Integer, String> result = TestService.getInstance().getTestList(offset, limit);
            if (result != null) {
                return new EnApiOutput(EnApiOutput.ERROR_CODE_API.SUCCESS, result);
            } else {
                return new EnApiOutput(EnApiOutput.ERROR_CODE_API.GET_LIST_TEST_FAIL);
            }
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
            return new EnApiOutput(EnApiOutput.ERROR_CODE_API.GET_LIST_TEST_FAIL);
        }
    }

}
