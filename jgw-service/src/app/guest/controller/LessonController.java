/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.guest.controller;

import app.guest.service.LessonService;
import app.entity.EnApiOutput;
import app.guest.service.VerifyUserNameService;
import app.config.ConfigApp;
import app.entity.EnApp;
import core.controller.ApiServlet;
import core.utilities.CommonUtil;
import java.util.ArrayList;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;

/**
 *
 * @author tam
 */
public class LessonController extends ApiServlet {

    private final Logger logger = Logger.getLogger(LessonController.class);
    VerifyUserNameService verifyInstance = VerifyUserNameService.getInstance(ConfigApp.LOGIN_SECRET_KEY);

    @Override
    protected EnApiOutput execute(HttpServletRequest req, HttpServletResponse resp) {
        try {
            if (!checkValidParam(req, new String[]{"action"})
                    || !CommonUtil.isValidString(req.getParameter("action"))) {
                logger.info("INVALID_DATA_INPUT ( Parameter : action): " + resp);
                return new EnApiOutput(EnApiOutput.ERROR_CODE_API.INVALID_DATA_INPUT);
            }
            String action = req.getParameter("action");
            switch (action) {
                case "getLessonList":
                    return getLessonList(req, resp);
                default:
                    return new EnApiOutput(EnApiOutput.ERROR_CODE_API.UNSUPPORTED_ERROR);
            }
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
        }
        return new EnApiOutput(EnApiOutput.ERROR_CODE_API.SERVER_ERROR);
    }

    private EnApiOutput getLessonList(HttpServletRequest req, HttpServletResponse resp) {
        try {
            ArrayList<EnApp.LessonInfo> result = LessonService.getInstance().getLessonList();
            if (result != null) {
                return new EnApiOutput(EnApiOutput.ERROR_CODE_API.SUCCESS, result);
            } else {
                return new EnApiOutput(EnApiOutput.ERROR_CODE_API.GET_LIST_LESSON_FAIL);
            }
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
            return new EnApiOutput(EnApiOutput.ERROR_CODE_API.GET_LIST_LESSON_FAIL);
        }
    }

}
