/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.user.service;

import app.config.ConfigApp;
import core.utilities.DBConnector;
import static hocdiban.tables.Result.RESULT;
import java.sql.Connection;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import org.apache.log4j.Logger;
import org.jooq.DSLContext;
import org.jooq.Record;
import org.jooq.SQLDialect;
import org.jooq.impl.DSL;
import org.json.JSONArray;
import hocdiban.tables.records.ResultRecord;

/**
 *
 * @author tam
 */
public class ResultService {
    
    private static final Logger logger = Logger.getLogger(ResultService.class.getName());
    private static final Lock LOCK = new ReentrantLock();
    private static ResultService instance = null;
    
    private final DBConnector dbConnector;
    
    private ResultService() {
        this.dbConnector = DBConnector.getInstance(ConfigApp.MYSQL_HOST,
                ConfigApp.MYSQL_PORT,
                ConfigApp.MYSQL_DBNAME,
                ConfigApp.MYSQL_USER,
                ConfigApp.MYSQL_PASSWORD);
    }
    
    public static ResultService getInstance() {
        if (instance == null) {
            LOCK.lock();
            try {
                if (instance == null) {
                    instance = new ResultService();
                }
            } finally {
                LOCK.unlock();
            }
        }
        return instance;
    }
    
    public Record insertResult(String userName, Integer testId, Double point, JSONArray answer) {
        Connection conn = null;
        try {
            conn = dbConnector.getMySqlConnection();
            DSLContext create = DSL.using(conn, SQLDialect.MYSQL);
            
            Record record = create.insertInto(RESULT, RESULT.USERNAME, RESULT.TESTID, RESULT.POINT, RESULT.ANSWER)
                    .values(userName, testId, point, answer.toString())
                    .returning(RESULT.ID)
                    .fetchOne();
            
            if (record != null) {
                logger.info("insert result success");
                return record;
            } else {
                logger.error("insert result fail");
                return null;
            }
            
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
        } finally {
            if (conn != null) {
                dbConnector.close(conn);
            }
        }
        return null;
    }
    
    public org.jooq.Result<ResultRecord> getResult(String userName) {
        Connection conn = null;
        try {
            conn = dbConnector.getMySqlConnection();
            DSLContext create = DSL.using(conn, SQLDialect.MYSQL);
            
            org.jooq.Result<ResultRecord> record = create.selectFrom(RESULT).where(RESULT.USERNAME.eq(userName)).fetch();
            if (record != null) {
                logger.info("get result success");
                return record;
            } else {
                logger.error("get result fail");
                return null;
            }
            
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
        } finally {
            if (conn != null) {
                dbConnector.close(conn);
            }
        }
        return null;
    }
}
