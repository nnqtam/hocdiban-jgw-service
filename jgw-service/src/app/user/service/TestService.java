/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.user.service;

import app.config.ConfigApp;
import app.entity.EnApp.*;
import com.google.gson.Gson;
import core.utilities.DBConnector;
import static hocdiban.Tables.TEST;
import static hocdiban.tables.Question.QUESTION;
import hocdiban.tables.records.QuestionRecord;
import hocdiban.tables.records.TestRecord;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import org.apache.log4j.Logger;
import org.jooq.DSLContext;
import org.jooq.SQLDialect;
import org.jooq.impl.DSL;

/**
 *
 * @author tam
 */
public class TestService {

    private static final Logger logger = Logger.getLogger(TestService.class.getName());
    private static final Lock LOCK = new ReentrantLock();
    private static TestService instance = null;

    private final DBConnector dbConnector;

    private TestService() {
        this.dbConnector = DBConnector.getInstance(ConfigApp.MYSQL_HOST,
                ConfigApp.MYSQL_PORT,
                ConfigApp.MYSQL_DBNAME,
                ConfigApp.MYSQL_USER,
                ConfigApp.MYSQL_PASSWORD);
    }

    public static TestService getInstance() {
        if (instance == null) {
            LOCK.lock();
            try {
                if (instance == null) {
                    instance = new TestService();
                }
            } finally {
                LOCK.unlock();
            }
        }
        return instance;
    }

    public Map getTest(int id) {
        Connection conn = null;
        try {
            conn = dbConnector.getMySqlConnection();
            DSLContext create = DSL.using(conn, SQLDialect.MYSQL);
            TestRecord testDetail = create.fetchOne(TEST, TEST.ID.eq(id));
            if (testDetail == null) {
                logger.error("can not get Test");
                return null;
            }

            Gson googleJson = new Gson();
            int[] listQuestionID = googleJson.fromJson(testDetail.getContent(), int[].class);
            ArrayList<String> listCorrectAnswer = new ArrayList<String>();
            ArrayList<EnQuestion> questions = new ArrayList<EnQuestion>();
            for (int i = 0; i < listQuestionID.length; i++) {
                int questionID = listQuestionID[i];
                QuestionRecord question = create.fetchOne(QUESTION, QUESTION.ID.eq(questionID));
                if (question != null) {
                    EnContentQuestion content = new EnContentQuestion();
                    String type = question.getType();
                    switch (type) {
                        case "CHOOSE_WORD":
                            content = googleJson.fromJson(question.getContent(), ChooseWord.class);
                            break;
                        case "PRONUNCIATION":
                            content = googleJson.fromJson(question.getContent(), Pronunciation.class);
                            break;
                        case "CHOOSE_PASSAGE":
                            content = googleJson.fromJson(question.getContent(), ChoosePassage.class);
                            break;
                        case "FIND_MISTAKE":
                            content = googleJson.fromJson(question.getContent(), FindMistake.class);
                            break;
                        case "REWRITE_1":
                            content = googleJson.fromJson(question.getContent(), Rewrite1.class);
                            break;
                        case "REWRITE_2":
                            content = googleJson.fromJson(question.getContent(), Rewrite2.class);
                            break;
                        default:
                            logger.error("Question type isn't exist - " + question.toString());
                            return null;
                    }
                    String correctAnswer = question.getCorrectanswer();
                    listCorrectAnswer.add(correctAnswer);
                    EnQuestion q = new EnQuestion(type, content, correctAnswer);
                    questions.add(q);
                } else {
                    logger.error("Can not get question");
                    return null;
                }
            }
            Map<String, Object> result = new HashMap<String, Object>();
            result.put("name", testDetail.getName());
            result.put("answer", listCorrectAnswer);
            result.put("content", questions);
            return result;
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
        } finally {
            if (conn != null) {
                dbConnector.close(conn);
            }
        }
        logger.error("Database Error");
        return null;
    }

}
