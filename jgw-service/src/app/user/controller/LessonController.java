/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.user.controller;

import app.user.service.LessonService;
import app.entity.EnApiOutput;
import app.entity.EnApp.LessonDetail;
import core.utilities.CommonUtil;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;

/**
 *
 * @author tam
 */
public class LessonController extends AbstractUserController {

    private final Logger logger = Logger.getLogger(LessonController.class);

    @Override
    protected EnApiOutput doProcess(String action, String verifiedUserName, HttpServletRequest req, HttpServletResponse resp) {
        try {
            switch (action) {
                case "getLesson":
                    return getLesson(req, resp);
                default:
                    return new EnApiOutput(EnApiOutput.ERROR_CODE_API.UNSUPPORTED_ERROR);
            }
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return new EnApiOutput(EnApiOutput.ERROR_CODE_API.SERVER_ERROR);
        }
    }

    private EnApiOutput getLesson(HttpServletRequest req, HttpServletResponse resp) {
        try {
            if (!checkValidParam(req, new String[]{"id"})
                    || !CommonUtil.isInteger(req.getParameter("id"))) {
                logger.info("getLesson fail: " + req);
                return new EnApiOutput(EnApiOutput.ERROR_CODE_API.INVALID_DATA_INPUT);
            }
            
            int id = Integer.parseInt(req.getParameter("id"));
            LessonDetail result = LessonService.getInstance().getLesson(id);
            if (result != null) {
                return new EnApiOutput(EnApiOutput.ERROR_CODE_API.SUCCESS, result);
            } else {
                return new EnApiOutput(EnApiOutput.ERROR_CODE_API.LESSONID_NOT_EXIST);
            }

        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
            return new EnApiOutput(EnApiOutput.ERROR_CODE_API.SERVER_ERROR);
        }
    }

}
