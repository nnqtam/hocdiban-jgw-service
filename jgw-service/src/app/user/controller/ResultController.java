/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.user.controller;

import app.entity.EnApiOutput;
import app.entity.EnApp.ResultTest;
import app.user.service.ResultService;
import core.utilities.CommonUtil;
import static hocdiban.tables.Result.RESULT;
import hocdiban.tables.records.ResultRecord;
import java.util.ArrayList;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.jooq.Record;
import org.json.JSONArray;

/**
 *
 * @author tam
 */
public class ResultController extends AbstractUserController {

    private final Logger logger = Logger.getLogger(ResultController.class);

    @Override
    protected EnApiOutput doProcess(String action,String userName, HttpServletRequest req, HttpServletResponse resp) {
        try {
            switch (action) {
                case "insertResult":
                    return insertResult(userName,req, resp);
                case "getListResult":
                    return getListResult(userName,req, resp);
                default:
                    return new EnApiOutput(EnApiOutput.ERROR_CODE_API.UNSUPPORTED_ERROR);
            }
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
        return new EnApiOutput(EnApiOutput.ERROR_CODE_API.SERVER_ERROR);
    }

    private EnApiOutput insertResult(String userName,HttpServletRequest req, HttpServletResponse resp) {
        try {
            if (!checkValidParam(req, new String[]{"testId", "point", "answer"})
                    || !CommonUtil.isInteger(req.getParameter("testId"))
                    || !CommonUtil.isDouble(req.getParameter("point"))) {
                logger.info("insertResult fail: " + req);
                return new EnApiOutput(EnApiOutput.ERROR_CODE_API.INVALID_DATA_INPUT);
            }
            Integer testId = Integer.parseInt(req.getParameter("testId"));
            Double point = Double.parseDouble(req.getParameter("point"));
            JSONArray answer = new JSONArray(req.getParameter("answer"));

            Record result = ResultService.getInstance().insertResult(userName, testId, point, answer);
            if (result != null) {
                return new EnApiOutput(EnApiOutput.ERROR_CODE_API.SUCCESS, result.getValue(RESULT.ID));
            } else {
                return new EnApiOutput(EnApiOutput.ERROR_CODE_API.INSERT_RESULT_FAIL);
            }
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
            return new EnApiOutput(EnApiOutput.ERROR_CODE_API.SERVER_ERROR);
        }
    }

    private EnApiOutput getListResult(String userName,HttpServletRequest req, HttpServletResponse resp) {
        try {
            ArrayList<ResultTest> listResult = new ArrayList<ResultTest>();
            org.jooq.Result<ResultRecord> result = ResultService.getInstance().getResult(userName);
            if (result != null) {
                for (ResultRecord r : result) {
                    listResult.add(new ResultTest(r.getTestid(), r.getPoint(), r.getAnswer()));
                }
                return new EnApiOutput(EnApiOutput.ERROR_CODE_API.SUCCESS, listResult);
            } else {
                return new EnApiOutput(EnApiOutput.ERROR_CODE_API.GET_LIST_RESULT_FAILD);
            }
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
            return new EnApiOutput(EnApiOutput.ERROR_CODE_API.SERVER_ERROR);
        }
    }

}
