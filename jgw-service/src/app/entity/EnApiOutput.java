/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.entity;

import core.controller.ApiOutput;

/**
 *
 * @author tamnnq
 */
public class EnApiOutput extends ApiOutput {

    public static enum ERROR_CODE_API {
        SUCCESS(1, "SUCCESS"),
        INVALID_DATA_INPUT(-1, "INVALID_INPUT"),
        UNSUPPORTED_ERROR(-2, "UNSUPPORTED_ERROR"),
        ACTION_INVALID(-3, "ACTION_INVALID"),
        PERMISSION_DENIED(-10, "PERMISSION_DENIED"),
        USERNAME_OR_PASSWORD_INVALID(-4, "USERNAME_OR_PASSWORD_INVALID"),
        TOKEN_INVALID(-5, "TOKEN_INVALID"),
        LOGIN_TOKEN_INVALID(-6, "LOGIN_TOKEN_INVALID"),
        USER_NOT_EXIST(-7, "USER_NOT_EXIST"),
        USER_EXIST(-8, "USER_EXIST"),
        TYPE_QUESTION_INVALID(-784, "TYPE_QUESTION_INVALID"),
        INSERT_TEST_FAIL(-995, "INSERT_TEST_FAIL"),
        TEST_CONTENT_INVALID(-765, "TEST_CONTENT_INVALID"),
        QUESTION_DATA_INVALID(-985, "QUESTION_DATA_INVALID"),
        ANSWER_INVALID(-764, "ANSWER_INVALID"),
        TESTID_NOT_EXIST(-652, "TESTID_NOT_EXIST"),
        LESSONID_NOT_EXIST(-456, "LESSONID_NOT_EXIST"),
        GET_LIST_TEST_FAIL(-777, "GET_LIST_TEST_FAIL"),
        GET_LIST_RESULT_FAILD(-234, "GET_LIST_RESULT_FAILD"),
        GET_LIST_LESSON_FAIL(-423, "GET_LIS_LESSON_FAIL"),
        GET_LIST_TYPE_LESSON_FAIL(-423, "GET_LIST_TYPE_LESSON_FAIL"),
        INSERT_QUESTION_FAIL(-710, "INSERT_QUESTION_FAIL"),
        OFFSET_OR_LIMIT_INVALID(-453, "OFFSET_OR_LIMIT_INVALID"),
        INSERT_RESULT_FAIL(-623, "INSERT_RESULT_FAIL"),
        INSERT_LESSON_FAIL(-635, "INSERT_LESSON_FAIL"),
        INSERT_TYPE_LESSON_FAIL(-335, "INSERT_TYPE_LESSON_FAIL"),
        DATABASE_ERROR(-999, "DATABASE_ERROR"),
        SERVER_ERROR(-1000, "SYSTEM_ERROR");

        public int code;
        public String message;

        private ERROR_CODE_API(int code, String message) {
            this.code = code;
            this.message = message;
        }
    }

    public EnApiOutput(ERROR_CODE_API result) {
        super(result.code, result.message, null);
    }

    public EnApiOutput(int code, String message, Object data) {
        super(code, message, data);
    }

    public EnApiOutput(ERROR_CODE_API result, Object data) {
        super(result.code, result.message, data);
    }

    /*
        API OUTPUT DEFINE
     */
}
