/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.entity;

import com.google.gson.Gson;
import java.sql.Date;
import org.apache.log4j.Logger;
import java.util.List;

/**
 *
 * @author tam
 */
public class EnApp {

    private static final Logger logger = Logger.getLogger(EnApp.class.getName());
    private static final Gson googleJson = new Gson();

    public static class EnUserSession {

        public String token;
        public String userName;
        public String permission;

        public EnUserSession(String token, String userName, String permission) {
            this.token = token;
            this.userName = userName;
            this.permission = permission;
        }
    }

    public static class EnUserPermission {

        String userName;
        String permission;

        public EnUserPermission(String userName, String permission) {
            this.userName = userName;
            this.permission = permission;
        }

    }

    public static class EnUser {

        public String userName;
        public String password;
        public String name;
        public Date dob;
        public String sexual;
        public String address;
        public String school;

        public EnUser(String userName, String password, String name, Date dob, String sexual, String address, String school) {
            this.userName = userName;
            this.password = password;
            this.name = name;
            this.dob = dob;
            this.sexual = sexual;
            this.address = address;
            this.school = school;
        }
    }

    public static class EnQuestion {

        String type;
        Object content;
        String correctAnswer;

        public EnQuestion(String type, Object content, String correctAnswer) {
            this.type = type;
            this.content = content;
            this.correctAnswer = correctAnswer;
        }
    }

    public static class EnContentQuestion {
    }

    private class Answer {

        String A;
        String B;
        String C;
        String D;

        public Answer(String A, String B, String C, String D) {
            this.A = A;
            this.B = B;
            this.C = C;
            this.D = D;
        }
    }

    public static class ChooseWord extends EnContentQuestion {

        String content;
        Answer answer;

        public ChooseWord(String content, Answer answer) {
            this.content = content;
            this.answer = answer;
        }
    }

    public static class Pronunciation extends EnContentQuestion {

        Answer answer;

        public Pronunciation(Answer answer) {
            this.answer = answer;
        }
    }

    public static class FindMistake extends EnContentQuestion {

        String content;

        public FindMistake(String content) {
            this.content = content;
        }
    }

    public static class Rewrite1 extends EnContentQuestion {

        String content;

        public Rewrite1(String content) {
            this.content = content;
        }
    }

    public static class Rewrite2 extends EnContentQuestion {

        String content;
        String startWith;

        public Rewrite2(String content, String startWith) {
            this.content = content;
            this.startWith = startWith;
        }
    }

    private static class QuestionPassage {

        String content;
        Answer answer;
        String correctAnswer;

        public QuestionPassage(String content, Answer answer, String correctAnswer) {
            this.content = content;
            this.answer = answer;
            this.correctAnswer = correctAnswer;
        }
    }

    public static class ChoosePassage extends EnContentQuestion {

        String passage;
        List<QuestionPassage> questions = null;

        public ChoosePassage(String passage) {
            this.passage = passage;
        }
    }

    public static class ResultTest {

        int testId;
        Double point;
        String[] answer;

        public ResultTest(int testId, Double point, String answer) {
            this.point = point;
            this.testId = testId;
            this.answer = googleJson.fromJson(answer, String[].class);
        }

    }

    public static class LessonInfo {

        int id;
        String type;
        int importance;
        String name;

        public LessonInfo(int id, String type, int importance, String name) {
            this.id = id;
            this.type = type;
            this.importance = importance;
            this.name = name;
        }
    }

    public static class LessonDetail {

        int id;
        String type;
        int importance;
        String name;
        String content;

        public LessonDetail(int id, String type, int importance, String name, String content) {
            this.id = id;
            this.type = type;
            this.importance = importance;
            this.name = name;
            this.content = content;
        }

    }

}
